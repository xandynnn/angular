var usersApp = angular.module('users', []);

usersApp.controller('UsersController', ['$scope', '$http', function($scope, $http){

	$http.get("users.json").success(function(data){
		$scope.users = data.users;
		$scope.titulo = data.users[data.users.length -1 ].name;
		$scope.predicate = '-id';
	});

	$scope.addUser = function ($index){

		$scope.users.push({
			id 		: ""+$scope.users.length -1,
			name 		: $scope.fieldName,
			username : $scope.fieldUserName,
			password	: $scope.fieldPassword
		});

		$scope.titulo = $scope.fieldName;

		$http.post('buildJson.php', { 'fullTable': $scope.users, 'table': 'users' } ).success(function(data){
			
			// Limpa os campos
			$scope.fieldName='';
			$scope.fieldUserName='';
			$scope.fieldPassword='';

		});

	}

	$scope.removeUser = function($uid){
		alert($uid);
	
    	$scope.users.splice($uid, 1);
		$http.post('buildJson.php', { 'fullTable': $scope.users, 'table': 'users' } ).success(function(data){
			$scope.titulo = $scope.users[$scope.users.length-1].name;
		});
	}

	$scope.findUser = function(user){

	}

}]);