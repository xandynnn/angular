<?php

$data = json_decode(file_get_contents("php://input"));
$json = $data->fullTable;
$table = $data->table;
$file = $table.".json";

$arr = array($table=>$json);

// Abre ou cria o arquivo bloco1.txt
// "a" representa que o arquivo é aberto para ser escrito
$fp = fopen($file, "w+");
 
// Escreve "exemplo de escrita" no bloco1.txt
$escreve = fwrite($fp, json_encode($arr) );
 
// Fecha o arquivo
fclose($fp);

?>