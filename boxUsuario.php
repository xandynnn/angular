				
				<!-- Controller -->
				<script src="users.controller.js"></script>

				<div ng-controller="UsersController">

					<!-- Titulo -->
					<div>
						<div class="col-md-12 col-sm-12 col-xs-12" >
							<div class="jumbotron">
								<h1>Last User: {{ titulo }}</h1>
								<p>Usuários existentes: <span ng-repeat="user in users">{{ user.name }}, </span></p>
							</div>
						</div><!-- Fim Titulo -->
					</div>
					
					<!-- Adicionar Usuário -->
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div>
							
							<h3>Adicionar Usuário</h3>
							
							<!-- Nome -->
							<div class="form-group">
								<input type="text" class="form-control" ng-model="fieldName" placeholder="Nome">
							</div>
							
							<!-- Username -->
							<div class="form-group">
								<input type="text" class="form-control" ng-model="fieldUserName" placeholder="Username">
							</div>
							
							<!-- Senha -->
							<div class="form-group">
								<input type="text" class="form-control" ng-model="fieldPassword" placeholder="Senha">
							</div>

							<!-- Adicionar Produto -->
							<button ng-click="addUser()" class="btn btn-default">Adicionar Usuário</button>
							
							<div class="message" ng-model="fieldMensagem"></div>

						</div>
						<!-- Buscar usuário -->
						<div>
							
							<h3>Buscar Usuário</h3>
							
							<!-- Nome -->
							<div class="form-group">
								<input type="text" class="form-control" ng-model="fieldSearch" placeholder="Digite o nome do usuário">
							</div>

						</div>
					</div>

					<!-- Remover Usuário -->
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h3>Remover Usuários</h3>
						
						<div class="panel panel-default">
						
							<div class="panel-heading">Lista de Usuários</div>

							<select data-ng-model="elementOrder">
								<option value="" selected>Default</option>
								<option value="name">Nome (ASCE)</option>
								<option value="-name">Name (DESC)</option>
							</select>

							<!-- Table -->
							<table class="table">
								<table class="table">
									<thead>
										<tr>
											<th>Itens Lista</th>
											<th>ID</th>
											<th>Nome</th>
											<th>Username</th>
											<th>Senha</th>
											<th>Ação</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="user in users | orderBy:elementOrder | filter : { name : fieldSearch } as results ">
											<td scope="row">{{$index + 1}}</td>
											<td>{{ user.id }}</td>
											<td>{{ user.name }}</td>
											<td>{{ user.username }}</td>
											<td>{{ user.password }}</td>
											<td><a href="javascript:void(0);" class="btn btn-danger" ng-click="removeUser($index)">Remover</a></td>
										</tr>
										<tr ng-if="results.length == 0">
											<td colspan="6">Não foi encontrado resultado</td>
										</tr>
									</tbody>
							</table>
						</div>

					</div><!-- Fim Remover Usuário -->

				</div>