﻿<html>
	<!-- documentacao -->
	<!-- https://docs.angularjs.org/api/ng/filter/orderBy -->
	<!-- Muito boa documentação com exemplos -> http://jptacek.com/2013/10/angularjs-introduction/ -->
	<!-- Exemplos no github https://github.com/jptacek/AngularPeriodic -->
	<head>
		<title>Cadastro de Usuários</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/alertify.core.css" />
		<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="js/alertify.js"></script>
		
	</head>
	<body ng-app="users">
		<div class="container">
			<div class="row">

				<?php require_once("boxUsuario.php"); ?>

			</div>
		</div>

	</body>
</html>